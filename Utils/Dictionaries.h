  std::map<int, std::string> set_external_trigger_select_description =
  {
  {0, "from DIO5"},
  {1, "from SFP"},
  {2, "from fmc l12 spare pin7"}
  };

    std::map<int, std::string> trigger_source_description =
  {
  {0, "none"},
  {1, "IPBus"},
  {2, "Test-FSM"},
  {3, "TTC"},
  {4, "TLU"},
  {5, "External"},
  {6, "Hit-Or"},
  {7, "User-defined frequency"}
  };

  const std::map<uint32_t, std::string> clocks_description =
  {
    {1, "ipb_clk"},
    {2, "fabric_clk"},
    {3, "clk_40MHz"},
    {4, "clk_80MHz"},
    {5, "clk_160MHz"},
    {6, "osc125_a_mgtrefclk_i"},
    {7, "fmc_l12_pll_clk"},
    {8, "clk_200MHz"},
    {9, "ref_clk_200MHz"}
  };

  std::map<int, std::string> channel_termination_description =
  {
    {0, "no channel with termination"},
    {1, "only L1"},
    {6, "channel 1"}, // 1-5 chanel enable / +5 terminator enable in that channel
    {7, "channel 2"},
    {8, "channel 3"},
    {9, "channel 4"},
    {10, "channel 5"},
    {31, "all channels"} // all of them 16 + 8 - 1 = 31
  };