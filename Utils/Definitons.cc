/*

   FileName :                    Decoder.cc
   Content :                     Handles with ...
   Programmer :                  
   Version :                     1.0
   Date of creation :            04/10/21
   Support :                     mail to : 

 */

#include "../Utils/Definitons.h"
#include <boost/format.hpp>

namespace  TTC_FC7_Utils 
{

  Decoder::Decoder(){
    //std::cout << " Using decoder...";
  }

  Decoder::~Decoder(){}

  void Decoder::hello_world()
  {
    std::cout << " Teste function...";
    return;
  } 

  struct tr_logger_sel_data Decoder::decode_log(uint32_t& tr_logger_block_raw, bool online_decoding)
  {

    const std::map<uint32_t, std::string> BC0_log_result = 
    {
      {0x0, (boost::format("%x") % (tr_logger_block_raw&0x1)).str()} // tr_logger_dout[i]&0x1
    };

    // make default result better
    std::map<uint32_t, struct tr_logger_sel_data > tr_logger_map_null = {{0x0, {"Err", "Err", "Err",    BC0_log_result, 0x0, 0x0, tr_logger_block_raw, "Err"}}};
    if (online_decoding)
    {

      const std::map<uint32_t, std::string> L1_log_result = 
      {
        {0x0, (boost::format("%05u") % (tr_logger_block_raw&0x7F)).str()} 
      };

      // --- full log dump map struct
      std::map<uint32_t, struct tr_logger_sel_data > tr_logger_map =
      {
        // tr_logger_sel {type_log  type_result1 type_result2 log_result_map result1 result2, raw_data, log_result} 
        {0x0, {"[BC0]", "ORB", "BC0_en",    BC0_log_result, (tr_logger_block_raw>>0x1)&0x3FFFF, 0x0,                      tr_logger_block_raw, "0"}},  
        {0x1, {"[L1A]", "BCN", "L1A phase", L1_log_result,  (tr_logger_block_raw>>0x7)&0xFFF,   0x0,                      tr_logger_block_raw, "0"}}, 
        {0x2, {"[CHB]", "BCN", "ChB Cmd",   CHB_log_result, (tr_logger_block_raw>>0x7)&0xFFF,   tr_logger_block_raw&0x7F, tr_logger_block_raw, "0"}},  // 22 bits --> | 3 bit sel | 12 bit bcnt+ChB_proc_sendahead_time-x"011" | 7 bit BGo_last_bcmd     |
        {0x3, {"[TTS]", "BCN", "TTS",       TTS_log_result, (tr_logger_block_raw>>0x7)&0xFFF,   tr_logger_block_raw&0x7F, tr_logger_block_raw, "0"}},  // 22 bits --> | 3 bit sel | 12 bit bcnt                                | 3 bit empty | 4 bit TTS |
        {0x4, {"[BGO]", "BCN", "BGO",       BGO_log_result, (tr_logger_block_raw>>0x7)&0xFFF,   tr_logger_block_raw&0x7F, tr_logger_block_raw, "0"}},  // 22 bits --> | 3 bit sel | 12 bit bcnt                                | 7 bit BGo               |
        {0x5, {"[L1R]", "BCN", "L1R phase", L1_log_result,  (tr_logger_block_raw>>0x7)&0xFFF,   0x0,                      tr_logger_block_raw, "0"}} 
      };
  
      uint32_t tr_logger_sel = (tr_logger_block_raw>>0x13)&0x7;
      if (tr_logger_map.find(tr_logger_sel) != tr_logger_map.end())
      {
        std::string log_result   = ((tr_logger_map[tr_logger_sel].type_log_result.find(tr_logger_map[tr_logger_sel].result2) != tr_logger_map[tr_logger_sel].type_log_result.end()) ? tr_logger_map[tr_logger_sel].type_log_result[tr_logger_map[tr_logger_sel].result2]  : "UNDEF: " + std::to_string(tr_logger_map[tr_logger_sel].result2));
        tr_logger_map[tr_logger_sel].log_result = log_result;
        return tr_logger_map[tr_logger_sel];
      }
      else 
      {
        return tr_logger_map_null[0];
      }

    } else return tr_logger_map_null[0];
  }
}