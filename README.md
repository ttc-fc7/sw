# TTC_FC7_Control

### Setup on AlmaLinux 9.2

1. Check which version of gcc is installed on your OS, it should be > 4.8:

        $> gcc --version

2. Install boost > v1.53 headers and pugixml as they don't ship with uHAL any more:

        $> sudo dnf install boost-devel
        $> sudo dnf install pugixml-devel

2. Install uHAL. SW tested with uHAL version up to 2.8.12-1

        Follow instructions from
        https://ipbus.web.cern.ch/ipbus/doc/user/html/software/install/yum.html

3. Install CERN ROOT

        $> sudo dnf install root
        $> sudo dnf install root-net-http root-net-httpsniff  root-graf3d-gl root-physics root-montecarlo-eg root-graf3d-eve root-geom libusb-devel xorg-x11-xauth.x86_64

5. Install CMAKE > 2.8:

        $> sudo dnf install cmake

### Install TTC_FC7_Control

        &> git clone https://gitlab.cern.ch/cms_tk_ph2/TTC_FC7_Control.git
        &> cd sw
        &> source setup.sh
        &> mkdir build
        &> cd build
        &> cmake ..
        &> make -j<number_of_cores>

### Running a FIFO readout test

        &> mkdir test
        &> cd test
        &> ttc_fc7_module_test --log-dump -c ../../settings/TTC_FC7Description_skeleton.xml
        &> ttc_fc7_module_test --log-dump-decoder -i ttc_log_dump/<file_name>.bin -c ../../settings/TTC_FC7Description_skeleton.xml

The options for running, including the IP address of the TTC-FC7, are found in `settings/TTC_FC7Description_skeleton.xml`.
