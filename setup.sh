#!/bin/bash

# CACTUS
#########################################################
export CACTUSBIN=/opt/cactus/bin
export CACTUSLIB=/opt/cactus/lib
export CACTUSINCLUDE=/opt/cactus/include
export CACTUSROOT=/opt/cactus/

# BOOST
#########################################################
export KERNELRELEASE=$(uname -r)
if [[ $KERNELRELEASE == *"el6"* ]]; then
    export BOOST_LIB=/opt/cactus/lib
    export BOOST_INCLUDE=/opt/cactus/include
elif [[ $KERNELRELEASE == *"el8"* ]]; then
    export BOOST_LIB="" #/opt/cactus/lib
    export BOOST_INCLUDE="" #/opt/cactus/include
elif [[ $KERNELRELEASE == "5."*"-generic" ]]; then
    export BOOST_INCLUDE=/usr/include
    export BOOST_LIB=/usr/lib/x86_64-linux-gnu
else
    export BOOST_INCLUDE=/usr/include
    export BOOST_LIB=/usr/lib64
fi

# Ph2_ACF
#########################################################
export TTC_FC7_BASE_DIR=$(pwd)

# System
#########################################################
export PATH=$TTC_FC7_BASE_DIR/bin:$PATH

# Flags
#########################################################
export HttpFlag='-D__HTTP__'

# Compilations
#########################################################
# Stand-alone application, without data streaming
#export CompileForHerd=false
#export CompileForShep=false

# Clang-format command
if command -v clang-format &> /dev/null; then
  clang_command="clang-format"
else
  clang_command="/opt/rh/llvm-toolset-7.0/root/usr/bin/clang-format"
fi

alias formatAll="find ${TTC_FC7_BASE_DIR} -iname *.h -o -iname *.cc | xargs ${clang_command} -i"

echo "=== DONE ==="
