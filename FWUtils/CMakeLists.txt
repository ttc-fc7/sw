MESSAGE(STATUS " ")
MESSAGE(STATUS "    ${BoldBlue}========================================================================================================${Reset}")
MESSAGE(STATUS "    ${BoldYellow}MIDDLEWARE${Reset} [stand-alone/middleware]: [${BoldCyan}TTC_FC7_Control/FWUtils/CMakeLists.txt${Reset}]. ${BoldRed}Begin...${Reset}")
MESSAGE(STATUS " ")

include_directories(${UHAL_UHAL_INCLUDE_PREFIX})
include_directories(${UHAL_GRAMMARS_INCLUDE_PREFIX})
include_directories(${UHAL_LOG_INCLUDE_PREFIX})
include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${PROJECT_SOURCE_DIR}/FWUtils)
include_directories(${PROJECT_SOURCE_DIR})

# Replace this with find_package
link_directories(${UHAL_UHAL_LIB_PREFIX})
link_directories(${UHAL_LOG_LIB_PREFIX})
link_directories(${UHAL_GRAMMARS_LIB_PREFIX})

# Find source files
file(GLOB HEADERS *.h)
file(GLOB SOURCES *.cc)

# Add the library
add_library(TTC_FC7_FWUtils STATIC ${SOURCES} ${HEADERS})

# Check for TestCard USBDriver
TARGET_LINK_LIBRARIES(TTC_FC7_FWUtils ${LIBS})

MESSAGE(STATUS " ")
MESSAGE(STATUS "    ${BoldYellow}MIDDLEWARE${Reset} [stand-alone/middleware]: [${BoldCyan}TTC_FC7_Control/FWUtils/CMakeLists.txt${Reset}]. ${BoldRed}DONE!${Reset}")
MESSAGE(STATUS "    ${BoldBlue}========================================================================================================${Reset}")
MESSAGE(STATUS " ")
