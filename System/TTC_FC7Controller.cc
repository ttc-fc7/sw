/*!
  \file                  TTC_FC7Controller.cc
  \brief                 Controller of the System, overall wrapper of the framework
  \author                Alexandra CARVALHO
  \version               0.001 (in preparation)
  \date                  not ready
  Support:               email to alexandra.oliveira@cern.ch
*/

#include "TTC_FC7Controller.h"
#include "../Utils/FileHandler_TTC_FC7.h"
#include "../Utils/Timer.h"
#include "../Utils/Utilities.h" // currentDateTime
#include "../Utils/Dictionaries.h" // Description of xml options
#include <iostream>
#include <iomanip>
#include <future>
#include <tuple>
#include <unistd.h>
#include <string>
#include <map>
#include <bitset>

#include <boost/format.hpp>

// interrupt handling
/////////////////////////////////////////////////////////////////////
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

bool doQuit=false;

void signal_callback_handler(int signum) {
  doQuit = true;
  std::cout << "" << std::endl;
  LOG(INFO) << BOLDRED << "FIFO log dump interrupted !" << RESET;
}
/////////////////////////////////////////////////////////////////////

using namespace std;
using namespace TTC_FC7_HwDescription;
using namespace TTC_FC7_HwInterface;
using namespace TTC_FC7_Utils; // Decoder, SettingsMap, tr_logger_sel_data

namespace TTC_FC7_System
{
    TTC_FC7Controller::TTC_FC7Controller()
        : fTTC_FC7Interface(nullptr)
        , fTTC_FC7FWInterface(nullptr)
        , fTTC_FC7(nullptr)
        , fFileHandler_TTC_FC7(nullptr)
        //, fWriteHandlerEnabled(false)
        , fRawFileName("")
        , trigger_source(-1)
    {
    }

    TTC_FC7Controller::~TTC_FC7Controller()
    {
        Destroy();
    }

    void TTC_FC7Controller::Inherit(const TTC_FC7Controller* pController)
    {
    }

    void TTC_FC7Controller::Destroy()
    {
        delete fTTC_FC7Interface;
        fTTC_FC7Interface = nullptr;
        delete fTTC_FC7FWInterface;
        fTTC_FC7FWInterface = nullptr;
        delete fTTC_FC7;
        fTTC_FC7 = nullptr;

        LOG(INFO) << BOLDRED << ">>> TTC_FC7Controller  destroyed <<<" << RESET;
    }

    void TTC_FC7Controller::InitializeHw(const std::string &pFilename, std::ostream &os)
    {
        this->fParser.parseHW(pFilename, fTTC_FC7FWInterface, fTTC_FC7, os);
        fTTC_FC7Interface = new TTC_FC7Interface(fTTC_FC7FWInterface, fTTC_FC7);
        std::vector<std::string> lstNames = fTTC_FC7Interface->getFpgaConfigList();
    }

    void TTC_FC7Controller::CreateResultDirectory(const std::string& pDirname, bool pDate) // bool pMode,
    {
        std::string fDirectoryName = "";
        // Fabio: CBC specific -> to be moved out from Tool_simple - BEGIN
        std::string nDirname = pDirname;
        if(pDate) nDirname += currentDateTime();
        LOG(INFO) << GREEN << "Creating directory: " << BOLDYELLOW << nDirname << RESET;
        std::string cCommand = "mkdir -p " + nDirname;
        try
        {
            system(cCommand.c_str());
        }
        catch(std::exception& e)
        {
            LOG(ERROR) << "Excepting when trying to create Result Directory: " << e.what();
        }

        fDirectoryName = nDirname;
    }

    void TTC_FC7Controller::InitializeSettings(const std::string &pFilename, std::ostream &os)
    {
        this->fParser.parseSettings(pFilename, fSettingsMap, os);
    }

    void TTC_FC7Controller::addFileHandler_TTC_FC7(const std::string& pFilename, char pOption)
    {
        if(pOption == 'r')
            fFileHandler_TTC_FC7 = new FileHandler_TTC_FC7(pFilename, pOption);
        else if(pOption == 'w')
        {
            fRawFileName         = pFilename;
            fWriteHandlerEnabled = true;
        }
    }

    void TTC_FC7Controller::closeFileHandler_TTC_FC7()
    {
        if(fFileHandler_TTC_FC7 != nullptr)
        {
            if(fFileHandler_TTC_FC7->isFileOpen() == true) fFileHandler_TTC_FC7->closeFile();
            delete fFileHandler_TTC_FC7;
            fFileHandler_TTC_FC7 = nullptr;
        }
    }

    void TTC_FC7Controller::initializeWriteFileHandler_TTC_FC7()
    {
        std::string cFilename = fRawFileName;
        fFileHandler_TTC_FC7 = new FileHandler_TTC_FC7(cFilename, 'w');
        //fBeBoardInterface->SetFileHandler_TTC_FC7(cBoard, fFileHandler_TTC_FC7); // link it to the board, that here is only one
        LOG(INFO) << GREEN << "Saving binary data into: " << RESET << BOLDYELLOW << cFilename << RESET;
    }

    void TTC_FC7Controller::print_version()
    {
      LOG(INFO) << YELLOW << " Version: " << RESET;
      std::map<std::string, uint32_t> version_firmware;
      const std::vector<std::string> version_firmware_vec = {"usr_ver_major", "usr_ver_minor", "usr_ver_build", "usr_firmware_dd", "usr_firmware_mm", "usr_firmware_yy"};
      for (std::string const&  xx : version_firmware_vec)
      {
        version_firmware[xx] = fTTC_FC7Interface->ReadBoardReg("user.stat_regs.usr_ver." + xx);
      }
      LOG(INFO) << YELLOW << " usr_ver_major/minor/build: " << version_firmware["usr_ver_major"] << "/" << version_firmware["usr_ver_minor"] << "/" << version_firmware["usr_ver_build"]  << RESET;
      LOG(INFO) << YELLOW << " Date: " << version_firmware["usr_firmware_dd"] << "/" << version_firmware["usr_firmware_mm"] << "/"<< version_firmware["usr_firmware_yy"] << RESET;
    }

    void TTC_FC7Controller::Start(int runNumber)
    {
        //for(auto cBoard: *fDetectorContainer) fTTC_FC7Interface->Start(cBoard);
    }

    void TTC_FC7Controller::Stop()
    {
        fTTC_FC7Interface->Stop();
    }

    void TTC_FC7Controller::Pause()
    {
        fTTC_FC7Interface->Pause();
    }

    void TTC_FC7Controller::Resume()
    {
        fTTC_FC7Interface->Resume();
    }

    void TTC_FC7Controller::Abort()
    {
        LOG(ERROR) << BOLDRED << __PRETTY_FUNCTION__ << " Abort not implemented" << RESET;
    }

    void TTC_FC7Controller::Reset()
    {
      LOG(INFO) << RED << " WARNING: reset not stable" << RESET;
      LOG(INFO) << GREEN << "      Resetting..." << RESET;
      LOG(INFO) << GREEN << "      - set_reset_all" << RESET;
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.reset_reg.global_rst", 1);
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.reset_reg.clk_gen_rst", 1);
      usleep(200000);
      LOG(INFO) << GREEN << "      - release_reset_all" << RESET;
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.reset_reg.global_rst", 0);
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.reset_reg.clk_gen_rst", 0);
      usleep(600000);
      LOG(INFO) << GREEN << "      Reset done" << RESET;
    }

    std::map<std::string, int> TTC_FC7Controller::count_lines(std::string file_name)
    {
      std::ifstream ff(file_name);
      std::string line;
      long ii;
      int iorbit = 0;
      for (ii = 0; std::getline(ff, line); ++ii)
      {
        if ( line.find( "ORB" ) != std::string::npos ) iorbit++;
      }
      return {
       {"nlines", ii},
       {"norbit", iorbit}
     };
    }

    void TTC_FC7Controller::empty_FIFO()
    {
      LOG(INFO) << GREEN << "Empty the FIFO 40 times without recording to arrive to a stable point." << RESET;
      for (int ii = 0; ii < 40; ii++ )
      {
        std::vector<uint32_t> tr_logger_block_read = fTTC_FC7Interface->ReadBlockBoardReg("user.stat_regs.tr_logger_dout", saturation_point_FIFO);
      }      
    }

    uint32_t TTC_FC7Controller::ReadData(std::vector<uint32_t>& pData, int& count_n_full, bool online_decoding, int verbose)
    {
      uint32_t cNPackets = fTTC_FC7Interface->ReadData(false, pData, count_n_full);
      if(cNPackets == 0) 
      {
        // cont_nempty
        return cNPackets;
      }

      if ( cNPackets > saturation_point_FIFO - 1 )
      {
        LOG(INFO) << BOLDRED << "    WARNING: FIFO was saturated, number of consecutive times = " << count_n_full <<  RESET;
        count_n_full++;
      } else count_n_full = 0;


      if ( pData.size() != cNPackets )
      {
        LOG(INFO) << "    WARNING: pData.size() != cNPackets (" << pData.size() << ", " << cNPackets << ")" << "\n";
      }

      if( verbose == 1 )
      {
        LOG(INFO) << YELLOW << "Number of state transition log entries since last FIFO emptying: " << cNPackets << RESET;
      }
      
      this->DecodeData(pData, online_decoding); //get list of decpded data, the struct I did
      // std::vector<uint32_t> tr_logger_block_read
      /*if (!online_decoding)
      {
        fEventList.clear();
        //for(auto& evt: pData) fEventList.push_back(&evt); // else this is going to be on the list of decoded data
        for(auto evt: pData) fEventList.push_back(evt);
      }*/
      return cNPackets;
    }

    void TTC_FC7Controller::DecodeData(const std::vector<uint32_t>& pData, bool online_decoding) // make offline option to save to a decoded file after, to xchack
    {
      // ####################
      // # Decoding TTC data #
      // ####################
      fEventList.clear();

      if (pData.size() > 0)
      {
        //Timer time_to_decode;
        //time_to_decode.start();
        //int counter_orbit = 0;
        //bool verbose = 0;
        bool test_read_write = false;
        
        for (uint32_t const&  yy : pData)
        {
          if ( test_read_write ) std::cout << this->textBinaryVersion(yy) << std::endl;
          uint32_t yy_test = yy;
          struct tr_logger_sel_data result_log_dump = decoder.decode_log(yy_test, online_decoding);
          fEventList.push_back(result_log_dump);
          //counter_orbit += counter_orbit_local;
        }
        //LOG(INFO) << YELLOW << "There was " << counter_orbit <<"(" << pData.size() << ")" << " orbits (entries) in the run." << RESET;
        //this->closeFileHandler_TTC_FC7();
        //time_to_decode.stop();
        //LOG(INFO) << YELLOW << "Time to decode: " << time_to_decode.getElapsedTime() << " us" << RESET;
      }


    }

    int TTC_FC7Controller::clean_FIFO(bool read_FIFO_size, int test_tr_logger_dcnt, std::vector<uint32_t> tr_logger_block_read, bool verbose)
    {
      int real_size_FIFO = 0;
      if ( !read_FIFO_size )
      {
        int count_repeated = 0;
        uint32_t tr_logger_block_raw_last;
        // loop to remove find the real FIFO size
        for (int ii = 0; ii < test_tr_logger_dcnt; ii++ )
        {
          uint32_t tr_logger_block_raw =  tr_logger_block_read[ii];
          if ( ii == 0 ) tr_logger_block_raw_last = tr_logger_block_raw;
          else
          {
            if (tr_logger_block_raw_last == tr_logger_block_raw) count_repeated++;
            else
            {
              count_repeated = 0;
              tr_logger_block_raw_last = tr_logger_block_raw;
            }
          }
          if ( count_repeated > tolerance_FIFO_repeat )
          {
            if ( verbose == 1 ) LOG(INFO) << YELLOW << "Will stoped recording FIFO in "<< real_size_FIFO << " as there was " << tolerance_FIFO_repeat << " consecutive entry repetitions. " << RESET;
            break;
          }
          real_size_FIFO++;
        }
      } else real_size_FIFO = tr_logger_block_read.size();
      return real_size_FIFO;
    }

    void TTC_FC7Controller::ttc_log_dump_offline_decoding(int verbose_loc, bool just_read)
    {
      // Conditions for collecting and saving data
      bool offline_decoding = TTC_FC7Controller::findValueInSettings("offline_decoding");
      bool log_in_binary = TTC_FC7Controller::findValueInSettings("log_in_binary");
      if ( !log_in_binary ) LOG(INFO) << BOLDRED << "Warning: It will save the raw data in text file format, you will not be able to post-decode it." << RESET;
      bool test_read_write = TTC_FC7Controller::findValueInSettings("test_read_write");
      bool decode_imediatelly = TTC_FC7Controller::findValueInSettings("decode_imediatelly");
      int max_entries = TTC_FC7Controller::findValueInSettings("max_entries"); // for tests
      int max_entries_by_file = TTC_FC7Controller::findValueInSettings("max_entries_by_file");
      bool read_FIFO_size = TTC_FC7Controller::findValueInSettings("read_FIFO_size");
      if ( !read_FIFO_size ) LOG(INFO) << BOLDRED << "Warning: It will not read the FIFO size on the FW before draining it, it will try to find the size by a pattern. That is under investigation/debugging. Not default behaviour." << RESET;
      
      //int max_file_num = TTC_FC7Controller::findValueInSettings("max_file_num");
      //int file_num = 1;

      // Conditions for collecting and saving diagnostics
      int verbose = verbose_loc > -1 ? verbose_loc : TTC_FC7Controller::findValueInSettings("verbose");
      bool test_timming  = TTC_FC7Controller::findValueInSettings("test_timming_ttc_log_dump");
      bool test_timming_per_read = TTC_FC7Controller::findValueInSettings("test_timming_per_read");
      int delay_between_reads = TTC_FC7Controller::findValueInSettings("delay_between_reads");// microseconds

      // interrupt handling
      signal(SIGINT, signal_callback_handler);

      //while ( file_num < max_file_num + 1 )
      while ( !doQuit )
      {
        Timer tt;
        Timer tt_by_read;
        Timer tt_only_read;
        Timer tt_by_loop;
        bool pDate = true;
        std::string ttc_log_dump_dir = "ttc_log_dump";
        TTC_FC7Controller::CreateResultDirectory(ttc_log_dump_dir);
        bool keep_saving = true;
        int total_entries = 0;
        int total_entries_file = 0;
        bool stop_run = false;

        std::string data_saving_format =  !offline_decoding ? ".decoded" : (log_in_binary ? ".bin" : ".raw") ;
        std::vector<tr_logger> ttc_log_dump_file_logs;
        std::vector<int> nentries;
        std::vector<float> time_between_reads;
        std::vector<float> time_per_read;
        std::vector<float> time_per_loop;
        bool reached_total_max = true;
        int count_n_full = 0;
        tt_by_read.start();

        int counter_orbit_run = 0;
        while ( reached_total_max && keep_saving && !doQuit )
        {
          int test_tr_logger_dcnt_by_file = 0;
          int count_n_empty = 0;

          std::string ttc_log_dump_file = std::string(ttc_log_dump_dir) + "/ttc_log_dump";
          if(pDate) ttc_log_dump_file += currentDateTime();

          tr_logger ttc_log_dump_file_log;
          ttc_log_dump_file_log.log_name = ttc_log_dump_file;
          ttc_log_dump_file += data_saving_format;
          this->addFileHandler_TTC_FC7(ttc_log_dump_file , 'w');
          this->initializeWriteFileHandler_TTC_FC7();
          if ( fFileHandler_TTC_FC7 == nullptr) 
          {
            LOG(ERROR) << BOLDRED << __PRETTY_FUNCTION__ << " File for log dump not opened." << RESET;
          }

          if ( test_timming ) tt.start();
          bool reached_file_max = false;
          while ( !reached_file_max && !stop_run && keep_saving && !doQuit )
          {
            if ( test_timming_per_read )
            {
              tt_by_read.stop();
              time_between_reads.push_back(tt_by_read.getElapsedTime());
              tt_by_read.start();
            }
            if ( test_timming_per_read ) tt_only_read.start();
            std::vector<uint32_t> cRawData(0);// = fTTC_FC7Interface->ReadBlockBoardReg("user.stat_regs.tr_logger_dout", tr_logger_dcnt);
            uint32_t test_tr_logger_dcnt = this->ReadData(cRawData, count_n_full, !offline_decoding, verbose); // count empty and full
            //fFileHandler_TTC_FC7->setData(cRawData);
          
            if ( count_n_full > 10 )
            {
              stop_run = true;
              LOG(INFO) << BOLDRED << "There was 10 consecutive reading attempts with saturated FIFO. Returning." << RESET;
            }

            if ( stop_run ) break;
      
            if ( test_timming_per_read ) tt_only_read.stop();
            time_per_read.push_back(tt_only_read.getElapsedTime());

            if (test_tr_logger_dcnt > 0)
            {
              std::vector<struct tr_logger_sel_data> cPh2NewEvents = this->GetEvents();
              count_n_empty = 0;

              tt_by_loop.start();
              int real_size_FIFO = clean_FIFO(read_FIFO_size, test_tr_logger_dcnt, cRawData, verbose);
              // In case we do not want to read the FIFO size (to save time) it was attempt to read always the saturated point, and remove the additional entries by putting a tolerance in repeated entries
              tt_by_loop.stop();
              time_per_loop.push_back(tt_by_loop.getElapsedTime());

              nentries.push_back(real_size_FIFO);
              total_entries += real_size_FIFO;
              total_entries_file += real_size_FIFO;
              test_tr_logger_dcnt_by_file += real_size_FIFO;

              // put the loop inside the writting
              for (struct tr_logger_sel_data cPh2Event : cPh2NewEvents)
              {
                if ( stop_run ) break;
                struct tr_logger_sel_data cPh2Event_loc = cPh2Event;
                int counter_orbit_local = this->ttc_log_dump_to_file(cPh2Event_loc, offline_decoding, log_in_binary, verbose, fFileHandler_TTC_FC7); 
                counter_orbit_run += counter_orbit_local;
              }

            } else
            {
              if ( verbose == 1 ) LOG(INFO) << BOLDRED << "There was nothing read on FIFO " << RESET;
              count_n_empty++;
            }

            if ( delay_between_reads > 0 ) usleep(delay_between_reads);
 
            if ( count_n_empty > 100 )
            {
              stop_run = true;
              LOG(INFO) << BOLDRED << "There was 100 consecutive reading attempts with empty FIFO. Returning." << RESET;
            }

            if (trigger_source == 1)
            {
              //keep_saving = false;
            }
            else {
              reached_file_max = (total_entries_file > max_entries_by_file);
              if ( reached_file_max ) total_entries_file = 0;
            }
          }
          this->closeFileHandler_TTC_FC7();
          tt.stop();

          ttc_log_dump_file_log.elapsed_time = tt.getElapsedTime();
          ttc_log_dump_file_log.test_tr_logger_dcnt_by_file = test_tr_logger_dcnt_by_file;
          ttc_log_dump_file_log.counter_orbit_by_file = 0; // TOFIX = remove from definition
          ttc_log_dump_file_logs.push_back(ttc_log_dump_file_log);

          if (trigger_source == 1) reached_total_max = true;
          else reached_total_max = (total_entries < max_entries) && max_entries != -1;

          this->closeFileHandler_TTC_FC7();
          LOG(INFO) << GREEN << "Result saved on file " << ttc_log_dump_file  << RESET;
        
          //file_num++;
        } // while total_entries < max_entries

        if (!offline_decoding) 
        {
          LOG(INFO) << GREEN << "There was " << counter_orbit_run << " orbits in the run." << RESET;
        }
      
        if ( test_timming_per_read ) tt_by_read.stop();
        time_between_reads.push_back(tt_by_read.getElapsedTime());

        if ( test_timming )
        {
          for (tr_logger const&  xx : ttc_log_dump_file_logs)
          {
            std::string ttc_log_dump_file_log = xx.log_name + ".log";
            std::string ttc_log_dump_file_raw = xx.log_name + data_saving_format;
            std::string ttc_log_dump_file_decoded = xx.log_name + ".decoded";
            int counter_orbit_by_file = 0;

            if ( log_in_binary && offline_decoding && decode_imediatelly )
            {
              LOG(INFO) << YELLOW << "" << RESET;
              LOG(INFO) << YELLOW << "Will read the above binary file and decode the content." << RESET;

              this->addFileHandler_TTC_FC7(ttc_log_dump_file_raw , 'r');
              std::vector<uint32_t> result = this->Read_bin_log(ttc_log_dump_file_raw);
              this->closeFileHandler_TTC_FC7();

              this->addFileHandler_TTC_FC7(ttc_log_dump_file_decoded , 'w');
              this->initializeWriteFileHandler_TTC_FC7();
              for (uint32_t const&  yy : result)
              {
                uint32_t yy_test = yy;
                struct tr_logger_sel_data result_log_dump = decoder.decode_log(yy_test, true);
                if ( result_log_dump.type_log != "Err" )
               {
                  int counter_orbit_local = this->Write_ttc_log_dump(result_log_dump, verbose, fFileHandler_TTC_FC7);
                  counter_orbit_by_file += counter_orbit_local;
                } else
                {
                  this->Write_raw_log(yy, true, verbose);
                  LOG(INFO) << BOLDRED << "Could not decode the entry. " << TTC_FC7Controller::textBinaryVersion(yy_test) << RESET;
                }
                if ( test_read_write ) std::cout << TTC_FC7Controller::textBinaryVersion(yy) << std::endl;
              }
              LOG(INFO) << YELLOW << "There was " << counter_orbit_by_file << " orbits in the file." << RESET;
              this->closeFileHandler_TTC_FC7();
            }

            int trigger_interval = fTTC_FC7Interface->ReadBoardReg("user.ctrl_regs.clocked_trigger.trigger_interval");
            int trigger_rate = 40000./trigger_interval;

            std::ofstream myfiletimming;
            myfiletimming.open(ttc_log_dump_file_log, ios::out);
            myfiletimming << "elapsed_time (s) :         " << xx.elapsed_time << "\n";
            myfiletimming << "entries produced :     " << xx.test_tr_logger_dcnt_by_file << "\n";
            if ( !log_in_binary )
            {
              std::map<std::string, int> num_lines = TTC_FC7Controller::count_lines(ttc_log_dump_file_raw);
              LOG(INFO) << YELLOW << "" << RESET;
              LOG(INFO) << YELLOW << "Output file  " << ttc_log_dump_file_log << "  has " << num_lines["nlines"] << " lines " << RESET;
              myfiletimming << "entries in log :       " << num_lines["nlines"] << "\n";
            }
            myfiletimming << "trigger rate (kHz) :     " << trigger_rate << "\n";
            myfiletimming << "nentries :     ";
            for (int const&  yy : nentries) myfiletimming << yy <<"  ";
            myfiletimming << "\n";
            if ( test_timming_per_read )
            {
              myfiletimming << "elapsed time_between_reads(us) :     ";
              for (float const&  yy : time_between_reads) myfiletimming << yy*1e+6 <<"  ";
              myfiletimming << "\n";

              myfiletimming << "elapsed time_per_read(us) :     ";
              for (float const&  yy : time_per_read) myfiletimming << yy*1e+6 <<"  ";
              myfiletimming << "\n";

              if ( !read_FIFO_size )
              {
              myfiletimming << "elapsed time_per_loop to remove duplicates(us) :     ";
              for (float const&  yy : time_per_loop) myfiletimming << yy*1e+6 <<"  ";
              myfiletimming << "\n";
              }
            }
            myfiletimming << "no of FIFO drains :     " << nentries.size() << "\n";
            if ( counter_orbit_by_file > 0 ) myfiletimming << "no of orbits read:     " << counter_orbit_by_file << "\n";
            myfiletimming.close();
            LOG(INFO) << GREEN << "Saving binary data into: " << RESET << BOLDYELLOW << ttc_log_dump_file_log << RESET;
          }
        }
      }
    } // ttc_log_dump_offline_decoding

    void TTC_FC7Controller::ttc_log_dump_offline_decoding_decoder(int verbose_loc, const std::string& pFilename)
    {
      // Conditions for collecting and saving data
      bool offline_decoding = TTC_FC7Controller::findValueInSettings("offline_decoding");
      bool log_in_binary = TTC_FC7Controller::findValueInSettings("log_in_binary");
      if ( !log_in_binary ) LOG(INFO) << BOLDRED << "Warning: It will save the raw data in text file format, you will not be able to post-decode it." << RESET;
      bool test_read_write = TTC_FC7Controller::findValueInSettings("test_read_write");
      bool read_FIFO_size = TTC_FC7Controller::findValueInSettings("read_FIFO_size");
      if ( !read_FIFO_size ) LOG(INFO) << BOLDRED << "Warning: It will not read the FIFO size on the FW before draining it, it will try to find the size by a pattern. That is under investigation/debugging. Not default behaviour." << RESET;
    
      // Conditions for collecting and saving diagnostics
      int verbose = verbose_loc > -1 ? verbose_loc : TTC_FC7Controller::findValueInSettings("verbose");
      //bool test_timming  = TTC_FC7Controller::findValueInSettings("test_timming_ttc_log_dump");
      // bool test_timming_per_read = TTC_FC7Controller::findValueInSettings("test_timming_per_read");
      //int delay_between_reads = TTC_FC7Controller::findValueInSettings("delay_between_reads");// microseconds

      std::string data_saving_format = !offline_decoding ? ".decoded" : (log_in_binary ? ".bin" : ".raw") ;
      std::string ttc_log_dump_file_raw = pFilename;
      std::string ttc_log_dump_file_decoded = pFilename + ".decoded";
      std::string ttc_log_dump_file_log = pFilename + ".log";
      int counter_orbit_by_file = 0;

      LOG(INFO) << YELLOW << "" << RESET;
      LOG(INFO) << YELLOW << "Will read the above binary file and decode the content." << RESET;

      this->addFileHandler_TTC_FC7(ttc_log_dump_file_raw , 'r');
      std::vector<uint32_t> result = this->Read_bin_log(ttc_log_dump_file_raw);
      this->closeFileHandler_TTC_FC7();

      this->addFileHandler_TTC_FC7(ttc_log_dump_file_decoded , 'w');
      this->initializeWriteFileHandler_TTC_FC7();
      for (uint32_t const&  yy : result)
        {
          uint32_t yy_test = yy;
          struct tr_logger_sel_data result_log_dump = decoder.decode_log(yy_test, true);
          if ( result_log_dump.type_log != "Err" )
          {
          int counter_orbit_local = this->Write_ttc_log_dump(result_log_dump, verbose, fFileHandler_TTC_FC7);
          counter_orbit_by_file += counter_orbit_local;
          } else
          {
          this->Write_raw_log(yy, true, verbose);
          LOG(INFO) << BOLDRED << "Could not decode the entry. " << TTC_FC7Controller::textBinaryVersion(yy_test) << RESET;
          }
          if ( test_read_write ) std::cout << TTC_FC7Controller::textBinaryVersion(yy) << std::endl;
        }
      LOG(INFO) << YELLOW << "There was " << counter_orbit_by_file << " orbits in the file." << RESET;
      this->closeFileHandler_TTC_FC7();
    }

    double TTC_FC7Controller::findValueInSettings(const std::string name, double defaultValue) const
    {
        auto setting = fSettingsMap.find(name);
        return (setting != std::end(fSettingsMap) ? setting->second : defaultValue);
    }

    void TTC_FC7Controller::Write_plain_log(std::string log, std::string after_str)
    {
      fFileHandler_TTC_FC7->appendFile(log, after_str);
    }

    void TTC_FC7Controller::Write_raw_log(uint32_t tr_logger_block_raw, bool err, int verbose)
    {
      std::bitset<32> tr_logger_block_raw_bin = std::bitset<8*sizeof(tr_logger_block_raw)>(tr_logger_block_raw);
      if (err) fFileHandler_TTC_FC7->appendFile("ERR! raw : ", " ");
      fFileHandler_TTC_FC7->appendRawToFile(tr_logger_block_raw_bin);
    }

    void TTC_FC7Controller::Write_bin_log(uint32_t tr_logger_block_raw) {
      fFileHandler_TTC_FC7->appendToBinFile(tr_logger_block_raw);
    }

    std::vector<uint32_t> TTC_FC7Controller::Read_bin_log(std::string file_to_read)
    {
      std::vector<uint32_t> result = fFileHandler_TTC_FC7->readFile_bin();
      return result;
    }

    std::string TTC_FC7Controller::textBinaryVersion(uint32_t myData)
    {
      std::string result = "";

      for (int i=0; i<32; ++i) {
        if ((myData>>i)&0x1) result = "1" + result;
        else result = "0" + result;
      }
      return result;
    }

    uint32_t TTC_FC7Controller::readValue(std::fstream &myFile, bool& goodRead)
    {
      uint32_t myData = 0;
      uint8_t aChar3;
      uint8_t aChar2;
      uint8_t aChar1;
      uint8_t aChar0;
      myFile >> aChar3;
      myFile >> aChar2;
      myFile >> aChar1;
      myFile >> aChar0;
      myData = (aChar3<<3*8) | (aChar2<<2*8) | (aChar1<<1*8) | aChar0;

      if (myFile.eof()) {
        goodRead=false;
        return 0;
      }
      return myData;
    }

    int TTC_FC7Controller::Write_ttc_log_dump(struct tr_logger_sel_data result, int verbose, FileHandler_TTC_FC7* &fFileHandler_TTC_FC7_internal)
    {
      int counter_orbit_local = 0;
      if ( verbose == 2 ) 
      {
        LOG(INFO) << YELLOW << result.type_log << ", " << result.type_result1 << "=" << result.result1 << ", "<< result.type_result2 << "=" << result.log_result << RESET; 
      }
      if ( fFileHandler_TTC_FC7_internal != nullptr)
      {
        fFileHandler_TTC_FC7_internal->appendFile(result.type_log, ", ");
        fFileHandler_TTC_FC7_internal->appendFile(result.type_result1, "=");
        fFileHandler_TTC_FC7_internal->appendFile(std::to_string(result.result1), ", ");
        fFileHandler_TTC_FC7_internal->appendFile(result.type_result2, "=");
        fFileHandler_TTC_FC7_internal->appendFile(result.log_result, "\n");
      }
      if ( result.type_result1 == "ORB" ) counter_orbit_local++;
      return counter_orbit_local;
    }

    int TTC_FC7Controller::ttc_log_dump_to_file(const struct tr_logger_sel_data entry, bool offline_decoding, bool log_in_binary, int verbose, FileHandler_TTC_FC7* &fFileHandler_TTC_FC7_internal) 
    {

      int counter_orbit_local = 0;
      //for (struct tr_logger_sel_data entry : decoded_FIFO)
      //{
        
        if (!offline_decoding)
        {
          fFileHandler_TTC_FC7_internal->appendFile(entry.type_log, ", ");
          fFileHandler_TTC_FC7_internal->appendFile(entry.type_result1, "=");
          fFileHandler_TTC_FC7_internal->appendFile(std::to_string(entry.result1), ", ");
          fFileHandler_TTC_FC7_internal->appendFile(entry.type_result2, "=");
          fFileHandler_TTC_FC7_internal->appendFile(entry.log_result, "\n");

          if ( verbose == 2 ) 
          {
            LOG(INFO) << YELLOW << entry.type_log << ", " << entry.type_result1 << "=" << entry.result1 << ", "<< entry.type_result2 << "=" << entry.log_result << RESET; 
          }
        } else if ( !log_in_binary)
        {
          uint32_t tr_logger_block_raw = entry.raw_data;
          std::bitset<32> tr_logger_block_raw_bin = std::bitset<8*sizeof(tr_logger_block_raw)>(tr_logger_block_raw);
          if (entry.type_log == "Err") fFileHandler_TTC_FC7_internal->appendFile("ERR! raw : ", " ");
          fFileHandler_TTC_FC7_internal->appendRawToFile(tr_logger_block_raw_bin);
        } else fFileHandler_TTC_FC7_internal->appendToBinFile(entry.raw_data);
        
        if ( entry.type_result1 == "ORB" ) counter_orbit_local++;
      //}
      return counter_orbit_local;

    }

} // namespace TTC_FC7_System
