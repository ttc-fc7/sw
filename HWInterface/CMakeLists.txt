
MESSAGE(STATUS " ")
MESSAGE(STATUS "    ${BoldBlue}========================================================================================================${Reset}")
MESSAGE(STATUS "    ${BoldYellow}MIDDLEWARE${Reset} [stand-alone/middleware]: [${BoldCyan}TTC_FC7_Control/HWInterface/CMakeLists.txt${Reset}]. ${BoldRed}Begin...${Reset}")
MESSAGE(STATUS " ")

# Includes
include_directories(${UHAL_UHAL_INCLUDE_PREFIX})
include_directories(${UHAL_GRAMMARS_INCLUDE_PREFIX})
include_directories(${UHAL_LOG_INCLUDE_PREFIX})
include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${PROJECT_SOURCE_DIR}/FWUtils)
include_directories(${PROJECT_SOURCE_DIR}/Utils)
include_directories(${PROJECT_SOURCE_DIR}/HWInterface)
include_directories(${PROJECT_SOURCE_DIR}/HWDescription)
include_directories(${PROJECT_SOURCE_DIR})

# Replace this with find_package
link_directories(${UHAL_UHAL_LIB_PREFIX})
link_directories(${UHAL_LOG_LIB_PREFIX})
link_directories(${UHAL_GRAMMARS_LIB_PREFIX})

# Find source files
file(GLOB HEADERS *.h)
file(GLOB SOURCES *.cc)

# Add the library
add_library(TTC_FC7_Interface STATIC ${SOURCES} ${HEADERS})

set(LIBS ${LIBS} TTC_FC7_Description TTC_FC7_FWUtils TTC_FC7_Utils cactus_uhal_uhal cactus_uhal_log boost_regex)

# Check for TestCard USBDriver
TARGET_LINK_LIBRARIES(TTC_FC7_Interface ${LIBS})

MESSAGE(STATUS " ")
MESSAGE(STATUS "    ${BoldYellow}MIDDLEWARE${Reset} [stand-alone/middleware]: [${BoldCyan}TTC_FC7_Control/HWInterface/CMakeLists.txt${Reset}]. ${BoldGreen}DONE!${Reset}")
MESSAGE(STATUS "    ${BoldBlue}========================================================================================================${Reset}")
MESSAGE(STATUS " ")
