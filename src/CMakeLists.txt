MESSAGE(STATUS " ")
MESSAGE(STATUS "    ${BoldYellow}========================================================================================================${Reset}")
MESSAGE(STATUS "    ${BoldYellow}MIDDLEWARE${Reset} [stand-alone/middleware]: [${BoldCyan}TTC_FC7_Control/src/CMakeLists.txt${Reset}]. ${BoldRed}Begin...${Reset}")
MESSAGE(STATUS " ")

# Includes
include_directories(${UHAL_UHAL_INCLUDE_PREFIX})
include_directories(${UHAL_GRAMMARS_INCLUDE_PREFIX})
include_directories(${UHAL_LOG_INCLUDE_PREFIX})
include_directories(${PROJECT_SOURCE_DIR}/HWDescription)
include_directories(${PROJECT_SOURCE_DIR}/HWInterface)
include_directories(${PROJECT_SOURCE_DIR}/FWUtils)
include_directories(${PROJECT_SOURCE_DIR}/Utils)
include_directories(${PROJECT_SOURCE_DIR}/System)
include_directories(${PROJECT_SOURCE_DIR})
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

# Library dirs
link_directories(${UHAL_UHAL_LIB_PREFIX})
link_directories(${UHAL_LOG_LIB_PREFIX})
link_directories(${UHAL_GRAMMARS_LIB_PREFIX})
link_directories(${PROJECT_SOURCE_DIR/lib})

# Initial set of libraries
set(LIBS ${LIBS} TTC_FC7_Description TTC_FC7_Interface TTC_FC7_FWUtils TTC_FC7_Utils TTC_FC7_System TTC_FC7_Tools )

#boost also needs to be linked
if(Boost_FOUND)
    set(LIBS ${LIBS} ${Boost_LIBRARIES})
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -lboost_date_time -lboost_iostreams")
endif()

####################################
## EXECUTABLES
####################################

file(GLOB BINARIES RELATIVE ${PROJECT_SOURCE_DIR}/src *.cc)

message("--     ${BoldCyan}#### Building the following executables: ####${Reset}")

foreach(sourcefile ${BINARIES})
    string(REPLACE ".cc" "" name ${sourcefile})
    message(STATUS "    ${name}")
    add_executable(${name} ${sourcefile})
    target_link_libraries(${name} ${LIBS})
endforeach(sourcefile ${BINARIES})

message("--     ${BoldCyan}#### End ####${Reset}")

MESSAGE(STATUS " ")
MESSAGE(STATUS "    ${BoldYellow}MIDDLEWARE${Reset} [stand-alone/middleware]: [${BoldCyan}TTC_FC7_Control/src/CMakeLists.txt${Reset}]. ${BoldGreen}DONE!${Reset}")
MESSAGE(STATUS "    ${BoldBlue}========================================================================================================${Reset}")
MESSAGE(STATUS " ")

