
#include "../Utils/Timer.h"
#include "../Utils/argvparser.h"
#include "../Utils/easylogging++.h"
#include "../System/TTC_FC7Controller.h"

#include <cstring>

using namespace TTC_FC7_HwDescription;
using namespace TTC_FC7_HwInterface;
using namespace TTC_FC7_System;
using namespace CommandLineProcessing;

INITIALIZE_EASYLOGGINGPP

int main(int argc, char* argv[])
{

    // configure the logger
    el::Configurations conf(std::string(std::getenv("TTC_FC7_BASE_DIR")) + "/settings/logger.conf");
    el::Loggers::reconfigureAllLoggers(conf);

    ArgvParser cmd;

    // init
    cmd.setIntroductoryDescription("CMS TTC_FC7 calibration example");
    // error codes
    cmd.addErrorCode(0, "Success");
    cmd.addErrorCode(1, "Error");
    // options
    cmd.setHelpOption("h", "help", "Print this help page");

    cmd.defineOption("config", "Hw Description File. Default: settings/TTC_FC7Description_skeleton.xml", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/);
    cmd.defineOptionAlternative("config", "c");

    cmd.defineOption("input", "Input path. Default: ttc_log_dump/ttc_log_dump.bin", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/);
    cmd.defineOptionAlternative("input", "i");

    cmd.defineOption("output", "Output Directory. Default: ttc_log_dump", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/);
    cmd.defineOptionAlternative("output", "o");

    cmd.defineOption("log-dump", "Empty the FIFO and print it in the terminal");
    cmd.defineOption("log-dump-decoder", "Decode the encoded FIFO log from .bin file");

    //cmd.defineOption("initialize", "Initialise board");

    //cmd.defineOption("configure", "Configure board");

    //cmd.defineOption("version", "Print firmware version");
    //cmd.defineOptionAlternative("version", "v");

    //cmd.defineOption("reset", "Reset board");


    int result = cmd.parse(argc, argv);

    if(result != ArgvParser::NoParserError)
    {
        LOG(INFO) << cmd.parseErrorDescription(result);
        exit(1);
    }

    // now query the parsing results
    std::string cHWFile = (cmd.foundOption("config")) ? cmd.optionValue("config") : "settings/TTC_FC7Description_skeleton.xml";
    std::string iLogFilePath = (cmd.foundOption("input")) ? cmd.optionValue("input") : "ttc_log_dump/ttc_log_dump.bin";

    // create a TTC_FC7Controller Object, I can then construct all other tools from that using the Inherit() method
    // this tool stays on the stack and lives until main finishes - all other tools will update the HWStructure from
    // cTool
    TTC_FC7Controller cTool;
    std::stringstream outp;
    cTool.InitializeHw(cHWFile, outp);
    cTool.InitializeSettings(cHWFile, outp);
    LOG(INFO) << outp.str();
    outp.str("");

    if (cmd.foundOption("log-dump")) cTool.ttc_log_dump_offline_decoding(1,true);
    if (cmd.foundOption("log-dump-decoder")) cTool.ttc_log_dump_offline_decoding_decoder(1, iLogFilePath);


    //if (cmd.foundOption("reset")) cTool.Reset();

    cTool.Start(0);
    cTool.Stop();

    //if(!batchMode) cApp.Run();
    return 0;
}
